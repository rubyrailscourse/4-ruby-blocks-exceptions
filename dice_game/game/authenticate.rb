module Authenticate

  def self.included base
    base.extend ClassMethods
  end

  module ClassMethods
    def authenticate! token
      token == valid_token[:digest] && valid_token[:expires_on] > Date.today
    end

    private
    def valid_token
      { digest: '9999', expires_on: Date.parse('15-02-2016') + 14 }
    end
  end
end
