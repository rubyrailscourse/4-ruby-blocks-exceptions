require_relative 'dice'

module Game
  class Main

    attr_accessor :score

    def initialize sides, num_dice
      @score = 0
      @dice = Dice.new sides
      @num_dice = num_dice
    end

    def to_s
      puts "You score: #{@score}"
    end

    def play rate
      sum = 0
      @num_dice.times{sum += @dice.throw}
      results(rate, sum)
    end

    def results(rate, result)
      if rate == result
        @score += 5
        puts "You Win! Score: #{@score}, rate: #{rate}, result: #{result}"
      else
        @score -= 1
        puts "You Lose! Score: #{@score}, rate: #{rate}, result: #{result}"
      end
    end
  end
end
