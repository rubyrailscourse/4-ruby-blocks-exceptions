module Game
  class Dice

    attr_accessor :sides

    def initialize sides
      @sides = sides
    end

    def throw
      rand(1..@sides)
    end
  end
end
