require 'date'
require_relative 'game/main'
require_relative 'game/authenticate'

module Game
 include Authenticate
  def self.create token, sides = 6, num_dice = 2
    if authenticate! token
      Main.new(sides, num_dice)
    else
      puts "Authentication error!!!"
    end
  end

  def self.version
    '1.0.0'
  end
end

game = Game.create('9999')
# game = Game.create('9999', 8, 3)
game.play(7)
game.play(6)
game.play(8)
game.play(5)
game.play(7)
game.play(8)
game.play(6)
game.play(8)
game.play(5)
game.play(7)
game.play(8)
