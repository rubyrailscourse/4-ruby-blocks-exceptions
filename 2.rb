# Подмешайте в класс Point модуль Comparable для получения методов сравнения
class Point
  include Comparable
  attr_accessor :x, :y

  def initialize x, y
    @x = x
    @y = y
  end

  def to_s
    puts "x = #{@x}, y = #{@y}"
  end

  def <=> second_point
    second_point.class == Point ? distance <=> second_point.distance : nil
  end

  def distance
    (x**2 + y**2)**0.5
  end
end

point1 = Point.new(2, 3)
point2 = Point.new(3, 5)
point3 = Point.new(4, 8)
point4 = 55
point3 <=> point4
point4 <=> point3
point1 <=> point2
point3 <=> point2
