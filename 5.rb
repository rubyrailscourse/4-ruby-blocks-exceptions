class Array
  def my_each
    for i in 0...self.length
      yield self[i]
    end
  end
end

numbers = [14, 21, 17, 65, 43]

numbers.my_each do |number|
  puts number
end
