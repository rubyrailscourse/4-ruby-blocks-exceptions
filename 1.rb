# Base64:
# Методы модуля для кодирования/декодирования строки в base64
require 'base64'
enc_name = Base64.encode64('Mikle')
dec_name = Base64.decode64(enc_name)

# Benchmark:
# Метод модуля, принимающий блок и вычисляющий время его выполнения
require 'benchmark'
puts Benchmark.measure { "b"*1_000_000 }

# Find:
# Метод модуля для рекурсивного обхода дерева каталогов относительно имени заданного
# каталога (переданного в качестве аргумента)
Find.find("ror_course") do |path|
  puts path
end

# Digest::MD5:
# Метод класса, вычисляющий md5 хэш от строки
require 'digest'
md5 = Digest::MD5.new
md5.update 'Tesla'
# --or--
Digest::MD5.hexdigest('Tesla')

# Timeout:
# Метод модуля, выкидывающий исключение, если код исполняемый
# в блоке вычисляется более N секунд
status = Timeout::timeout(2) {sleep 3}
